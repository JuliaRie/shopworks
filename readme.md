# Portfolio project for Shopworks

If you want to change any sass files

```bash
cd shopworks
npm install
bower install
```

Finally, run `npm start` to run the Sass compiler. It will re-run every time you save a Sass file.
![screenshot.jpg](https://bitbucket.org/repo/qakej9/images/1422712604-screenshot.jpg)